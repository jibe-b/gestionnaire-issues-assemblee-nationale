# Un gestionnaire d'issues pour l'Assemblée Nationale

L'Assemblée Nationale a un gestionnaire de questions écrites posées par les député·e·s [ici](http://www2.assemblee-nationale.fr/recherche/resultats_questions).

Seul·e·s les élue·e·s peuvent y écrire.

Ce prototype d'extension de navigateur ajoute un gestionnaire d'issues (et donc de questions) dans lequel toute personne peut écrire.
